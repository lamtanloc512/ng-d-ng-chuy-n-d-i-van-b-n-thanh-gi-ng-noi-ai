import "./App.css";
import { Form } from "react-bootstrap";
import Select from "react-select";
import { useState } from "react";

function App() {
    const gVoidOptions = [
        { value: "hn-quynhanh", label: "Giọng nói miền Bắc" },
        { value: "hue-baoquoc", label: "Giọng nói miền Trung" },
        { value: "hcm-diemmy", label: "Giọng nói miền Nam" },
    ];
    const [userText, setUserText] = useState("");
    const [voiceSelected, setVoiceSelected] = useState("hn-quynhanh");
    const handleSelectVoiceChange = (e) => {
        setVoiceSelected(e.value);
    };

    const [url, setUrl] = useState("");
    //handle send chat
    const handleSendChatText = () => {
        function fetchSpeechApi(paramTextToSend, paramVoiceToChoose) {
            let myHeaders = new Headers();
            myHeaders.append(
                "token",
                "PfVgD7TvTGFBYGfuUA9bnnWqkClxi7CVSuRgt2aS-juDOpQ88fiMYaa87IlNlRk3"
            );
            myHeaders.append("Content-Type", "application/json");
            let raw = JSON.stringify({
                text: paramTextToSend,
                voice: paramVoiceToChoose,
                id: "2",
                without_filter: false,
                speed: 1,
                tts_return_option: 2,
            });

            let requestOptions = {
                method: "POST",
                headers: myHeaders,
                body: raw,
                redirect: "follow",
            };

            fetch("https://viettelgroup.ai/voice/api/tts/v1/rest/syn", requestOptions)
                .then((response) => response.body)
                .then((body) => {
                    const reader = body.getReader();
                    return new ReadableStream({
                        start(controller) {
                            return pump();
                            function pump() {
                                return reader.read().then(({ done, value }) => {
                                    if (done) {
                                        controller.close();
                                        return;
                                    }
                                    controller.enqueue(value);
                                    return pump();
                                });
                            }
                        },
                    });
                })
                .then((stream) => new Response(stream))
                .then((response) => response.blob())
                .then((blob) => {
                    let file = new File([blob], "audio.mp3", { type: "audio/mpeg" });
                    return URL.createObjectURL(file);
                })
                .then((url) => {
                    return setUrl(url);
                })
                .catch((err) => console.error(err.text()));
        }
        fetchSpeechApi(userText, voiceSelected);
        setUserText("");

        console.log(url);
    };

    return (
        <div className="container d-flex align-items-center mt-5" style={{ paddingTop: "100px" }}>
            <div className="row w-100">
                <div className="col-12 col-md-7 col-lg-5 col-xl-4 mx-auto my-auto">
                    <div className="card shadow">
                        <div className="card-header py-4 bg-white border-0">
                            <div className="row">
                                <div className="col-3 me-2">
                                    <div style={{ width: "80px" }} className="rounded">
                                        <img
                                            className="img-fluid rounded"
                                            src="./logo512.png"
                                            alt="logo__ironhack"
                                        />
                                    </div>
                                </div>
                                <div className="col d-flex flex-wrap align-items-center">
                                    <h2 className="fw-bolder col-12 my-0 py-0">IRONHACK</h2>
                                    <h5 className="my-0 p-0">VIETNAM</h5>
                                </div>
                            </div>
                        </div>
                        <div className="card-body">
                            <div className="mt-3 mb-2">
                                <Select
                                    options={gVoidOptions}
                                    isClearable
                                    placeholder="Chọn giọng nói ..."
                                    onChange={handleSelectVoiceChange}
                                />
                            </div>

                            <br />
                            <Form.Group
                                className="mb-3 position-relative"
                                controlId="textarea__answer"
                            >
                                <Form.Label className="">Bạn hãy chat với tôi:</Form.Label>
                                <Form.Control
                                    value={userText}
                                    className="position-relative"
                                    as="textarea"
                                    rows={8}
                                    onChange={(e) => setUserText(e.target.value)}
                                />
                                <button
                                    className="btn btn-success rounded-circle position-absolute fw-bold"
                                    style={{
                                        width: "50px",
                                        height: "50px",
                                        bottom: "10px",
                                        right: "10px",
                                        fontSize: "9px",
                                    }}
                                    onClick={handleSendChatText}
                                >
                                    Send
                                </button>
                            </Form.Group>
                            <div className="row mt-2">
                                <div className="col-12">
                                    <audio
                                        id="response__audio"
                                        src={url}
                                        controls
                                        autoPlay
                                        className="mt-3 mb-1"
                                        style={{ width: "100%" }}
                                    />
                                </div>
                                <div className="col d-flex justify-content-center mt-3">
                                    {url !== "" ? (
                                        <a
                                            id="link__download"
                                            download="audio.mp3"
                                            href={url}
                                            className="btn btn-outline-primary fw-bold mt-2 w-100"
                                        >
                                            Tải về câu trả lời
                                        </a>
                                    ) : (
                                        ""
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default App;
